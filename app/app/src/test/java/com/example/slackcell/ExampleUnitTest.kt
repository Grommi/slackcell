package com.example.slackcell

import android.os.Environment
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mockito.*
import java.io.File


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun parseForceBufferSingleDataPoint() {
        val datapoints = parseForceBuffer("asdf*T123F456~sfasT789F123~*T5T548F123~*T875F45F574~*T12345678921F15026~*T12F0~*T123F145625~*T456F478~*T123F456~")
        assertEquals(3, datapoints.size)
        assertEquals(Datapoint(123, 456), datapoints[0])
    }
    @Test
    fun writeFile(){
        createFile("test.csv")
    }
}
