package com.example.slackcell

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast


import kotlinx.android.synthetic.main.activity_main.*
import mu.KLogging
import mu.KotlinLogging
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.util.*
import org.jetbrains.anko.toast
import kotlin.collections.ArrayList

private val log = KotlinLogging.logger {}
const val EXTRA_MESSAGE = "com.example.slackcell.MESSAGE"
val REQUEST_ENABLE_BT = 0

class MainActivity : AppCompatActivity() {
    private var m_bluetoothAdapter: BluetoothAdapter? = null
    private lateinit var m_pairedDevices: Set<BluetoothDevice>
    private val REQUEST_ENABLE_BLUETOOTH = 1

    companion object {
        val EXTRA_ADDRESS: String = "Device_address"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        m_bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if(m_bluetoothAdapter == null){
            toast("Bluetooth not supported")
            return
        }
        if(!m_bluetoothAdapter!!.isEnabled) {
            val enableBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBluetoothIntent, REQUEST_ENABLE_BLUETOOTH)
        }
        pairedDeviceList()
        select_device_refresh.setOnClickListener{pairedDeviceList()}
        test.setOnClickListener{test()}
    }

    private fun test(){
        createFile("test23.csv")
        writeDatapoint("test23.csv", Datapoint(123,456))
        readFile(" Test.csv")
    }

    private fun pairedDeviceList(){
        m_pairedDevices = m_bluetoothAdapter!!.bondedDevices
        val list : ArrayList<BluetoothDevice> = ArrayList()
        val name_list: ArrayList<String> = ArrayList()
        if (m_pairedDevices.isNotEmpty()){
            for (device: BluetoothDevice in m_pairedDevices){
                list.add(device)
                name_list.add(device.name + " (" + device.address + ")")
                Log.i("device", ""+device)
            }
        } else {
            toast("No paired devices found")
        }
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, name_list)
        select_bt_list.adapter = adapter
        select_bt_list.onItemClickListener = AdapterView.OnItemClickListener { _, _, index, _ ->
            val device: BluetoothDevice = list[index]
            val address: String = device.address
            val intent = Intent(this, ControlActivity::class.java)
            intent.putExtra(EXTRA_ADDRESS, address)
            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
            if (resultCode == Activity.RESULT_OK) {
                if (m_bluetoothAdapter!!.isEnabled) {
                    toast("Bluetooth enabled")
                } else {
                    toast("Bluetooth not enabled")
                }

            } else if (resultCode == Activity.RESULT_CANCELED) {
                toast("Bluetooth enabling cancelled")
            }
        }
    }
}
