package com.example.slackcell

import android.Manifest
import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.control_layout.*
import org.jetbrains.anko.toast
import java.io.File
import java.io.IOException
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.math.abs


class ControlActivity: AppCompatActivity(){
    var force = 0
    var maxForce = 0
    var recording = false
    var filename = ""
    lateinit var buttonRecord: Button

    companion object {
        var m_myUUID: UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
        var m_bluetoothSocket: BluetoothSocket? = null
        lateinit var m_progress: ProgressDialog
        lateinit var m_bluetoothAdapter: BluetoothAdapter
        var m_isConnected: Boolean = false
        lateinit var m_address: String
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1);
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1);
        }
        setContentView(R.layout.control_layout)
        m_address = intent.getStringExtra(MainActivity.EXTRA_ADDRESS)
        ConnectToDevice(this).execute()
        val handler = Handler()

        val textForce = findViewById<TextView>(R.id.text_force)
        val textForceMax = findViewById<TextView>(R.id.text_force_max)
        buttonRecord = findViewById<Button>(R.id.control_record_on)
        control_record_on.setOnClickListener{ startRecording() }
        control_record_off.setOnClickListener{ stopRecording() }
        control_disconnect.setOnClickListener{ disconnect() }
        resetMax.setOnClickListener{ resetMax() }

        val timer = Timer()
        val task = object: TimerTask() {
            override fun run() {
                val bt = readBT()
                val datapoints = parseForceBuffer(bt)
                if (datapoints.size > 0) {
                    for (point in datapoints) {
                        if (recording) {
                            writeDatapoint(filename, point)
                        }
                        if (abs(point.force) > maxForce) {
                            maxForce = abs(point.force)
                        }
                    }
                    handler.post {
                        textForce.text = datapoints.last().force.toString()
                        textForceMax.text = maxForce.toString()
                    }
                }
            }
        }
        timer.schedule(task, 100, 50)
    }

    private fun startRecording() {
        sendCommand('R')
        this.recording = true
        setButtonColor(this.buttonRecord, "#ff0000")
        this.filename = "SlackCell_" + DateTimeFormatter
            .ofPattern("yyyy-MM-dd_HH:mm:ss")
            .withLocale( Locale.GERMANY )
            .withZone( ZoneId.of("UTC"))
            .format(Instant.now()) + ".csv"
        createFile(this.filename)
        toast("Create $filename")
//        writeDatapoint(this.filename)
    }

    private fun stopRecording(){
        this.recording = false
        sendCommand('S')
        setButtonColor(buttonRecord, "#222222")
    }

    private fun sendCommand(command: Char, data: String = "") {
        val cmd = "*" + command + data.length + data + "~"
        Log.i("BT data", cmd)
        if(m_bluetoothSocket != null) {
            try{
                m_bluetoothSocket!!.outputStream.write(cmd.toByteArray())
            } catch(e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun resetMax() {
        val textForceMax = findViewById<TextView>(R.id.text_force_max)
        textForceMax.text = "-"
        maxForce = 0
    }

    private fun disconnect(){
        if(m_bluetoothSocket != null) {
            try {
                m_bluetoothSocket!!.close()
                m_bluetoothSocket = null
                m_isConnected = false
            } catch (e: IOException) {
                e.printStackTrace()
            }
            finish()
        }
    }

    private fun readForce(minBytes: Int = 8): String?{
        var command: String
        var btChar: Char = '0'
        if(m_bluetoothSocket != null){
            if(m_bluetoothSocket!!.inputStream != null) {
                val stream = m_bluetoothSocket!!.inputStream
                if (stream.available() > 0) {
                    Log.i("Available bytes", stream.available().toString())
                    // search for beginning of command
                    // TODO abort searching after a while
                    while (stream.available() > minBytes && btChar != '*') {
                        btChar = stream.read().toChar()
                        // read full command
                    }
                    command = "*"
                    try {
                        while (btChar != '~') {
                            btChar = stream.read().toChar()
                            command += btChar
                        }

                    } catch (e: IOException) {
                        e.printStackTrace()
                        toast("Bad BT command!")
                        return null
                    }

                    toast(command)
                    return command
                }
            }
        }
        return null
    }

    private fun readBT(): String {
        var numBytes: Int = 0 // bytes returned from read()
        var mmBuffer: ByteArray = ByteArray(1024)
        if(m_bluetoothSocket != null) {
            val inStream = m_bluetoothSocket!!.inputStream
            if (inStream != null) {
                while (true) {
                    try {
                        //reading data from input stream
                        numBytes = inStream.read(mmBuffer)
                        if (mmBuffer != null && numBytes > 0) {
                            val str = mmBuffer.toString()
                            return String(mmBuffer) //.toString() //.substring(0, numBytes - 1)
                            //Parse received bytes
                        }
                    } catch (e: IOException) {
                        //Error
                    }
                }
                // Keep listening to the InputStream until an exception occurs.
//                while (true) {
//                    // Read from the InputStream.
//                    numBytes = try {
//                        m_bluetoothSocket!!.inputStream.read(mmBuffer)
//                    } catch (e: IOException) {
//                        Log.d("Reading BT:", "Input stream was disconnected", e)
//                        return mmBuffer.sliceArray(0 until numBytes - 1).toString()
//                    }
//                }
            }
        }
        return ""
    }



    private fun parseForce(command: String): Pair<Int, Int> {
        val startTime = command.indexOf('T') + 1
        val startForce = command.indexOf('F') + 1
        val time = command.substring(startTime, startForce - 1).toInt()
        val force = command.substring(startForce, command.length - 1).toInt()

        return Pair(time, force)
    }


    private class ConnectToDevice(c: Context) : AsyncTask<Void, Void, String>(){
        private var connectSuccess: Boolean = true
        private val context: Context

        init {
            this.context = c
        }

        override fun onPreExecute() {
            super.onPreExecute()
            m_progress = ProgressDialog.show(context, "Connecting...", "please wait")
        }

        override fun doInBackground(vararg params: Void?): String? {
            try {
                if (m_bluetoothSocket == null || !m_isConnected) {
                    m_bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
                    val device: BluetoothDevice = m_bluetoothAdapter.getRemoteDevice(m_address)
                    m_bluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(m_myUUID)
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery()
                    m_bluetoothSocket!!.connect()
                }
            } catch (e: IOException) {
                connectSuccess = false
                e.printStackTrace()
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            if (!connectSuccess) {
                Log.i("data", "couldn't connect")
            } else {
                m_isConnected = true
            }
            m_progress.dismiss()
        }
    }

}