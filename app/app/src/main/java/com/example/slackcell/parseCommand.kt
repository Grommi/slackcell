package com.example.slackcell

import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.os.Environment
import android.util.Log
import android.widget.Button
import java.io.*

data class Datapoint(val time: Int, val force: Int)

fun parseForceBuffer(command: String): ArrayList<Datapoint> {
    val regex = Regex("[*][T]\\d{3,}[F]\\d{1,5}[~]")
    val matches = regex.findAll(command)
    val datapoints = ArrayList<Datapoint>()
    for (m in matches) {
        val match = m.value
        try {
            val time = match.substringAfter('T').substringBefore('F').toInt()
            val force = match.substringAfter('F').substringBefore('~').toInt()
            datapoints.add(Datapoint(time, force))
        } catch (e: NumberFormatException) {
            print("Can't parse $match")
        }
    }
    return datapoints
}

fun createFile(filename: String) {
    val sd_main = File(Environment.getExternalStorageDirectory(), "SlackCell")
    var success = true
    if (!sd_main.exists()) {
        success = sd_main.mkdir()
    }
    if (success) {
        // directory exists or already created
        val dest = File(sd_main, filename)
        try {
            // response is the data written to file
            dest.bufferedWriter().use { out -> out.write("time,force\n") }
        } catch (e: Exception) {
            // handle the exception
        }
//
    } else {
        Log.e("Recording", "Couldn't create directory.")
//        Toast.makeText(, "", Toast.LENGTH_SHORT).show()
    }

}


fun writeDatapoint(filename: String, datapoint: Datapoint){
    val dest = File(File(Environment.getExternalStorageDirectory(), "SlackCell"), filename)
    if (dest.exists()) {
        try {
            // response is the data written to file
            FileOutputStream(dest, true).bufferedWriter().use { out -> out.write(datapoint.time.toString() + "," + datapoint.force.toString() + "\n") }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }else {
    // directory creation is not successful
    }
}

fun readFile(filename: String) {

    val dest = File(File(Environment.getExternalStorageDirectory(), "SlackCell"), filename)
    var fileReader: BufferedReader? = null
    val la = File(Environment.getExternalStorageDirectory(), "SlackCell").walk()

    try {
        var line: String?

        fileReader = BufferedReader(FileReader(dest))

        // Read CSV header
        line = fileReader.readLine()

        // Read the file line by line starting from the second line
        line = fileReader.readLine()
        while (line != null) {
            //do your stuff
            }

    } catch (e: Exception) {
        println("Reading CSV Error!")
        e.printStackTrace()
    } finally {
        if (fileReader != null) {
            try {
                fileReader!!.close()
            } catch (e: IOException) {
                println("Closing fileReader Error!")
                e.printStackTrace()
            }
        }
    }
}

fun setButtonColor(btn: Button, color: String) {
    val iColor = Color.parseColor(color)
    if (Build.VERSION.SDK_INT >= 29)
        btn.background.colorFilter = BlendModeColorFilter(iColor, BlendMode.MULTIPLY)
    else
        btn.background.setColorFilter(iColor, PorterDuff.Mode.MULTIPLY)
}