//https://github.com/MajicDesigns/Chroniker
#include <SoftwareSerial.h>
#include <LinkedList.h>

const char PKT_START = '*';    // protocol packet start character
const char PKT_END = '~';      // protocol packet end character

const char ERR_OK = '0';
const char ERR_TIMEOUT = '1';
const char ERR_CMD = '2';
const char ERR_DATA = '3';
const char ERR = '4';

const uint8_t MAX_DATA_LEN = 10;
const uint16_t BT_COMMS_TIMEOUT = 1000; // Protocol packet timeout period (start to end packet within this period)

    typedef struct
{
  uint8_t cmd = '?';
  uint8_t dataLen;
  uint8_t data[MAX_DATA_LEN];
} cmd_t;

    class BT
{
  public:
  BT(uint8_t pinRX, uint8_t pinTX, uint16_t baud):
    _pinRX(pinRX), _pinTX(pinTX), _baud(baud)
    {
      _bt = new SoftwareSerial(_pinTX, _pinRX);
    };

  ~BT()
  {
    delete _bt;
  };

  void begin(){
    _bt->begin(_baud);
  }
  int read(){
    return _bt->read();
  }

  int available(){
    return _bt->available();
  }

  void setName(){
    _bt->write("AT+NAME=Poi\r\n");
  }

  void setPassword(){
    _bt->write("AT+PSWD=SlackCell\r\n");
  }

  void setBaudrate(){
    _bt->write("AT+UART=38400,0,0\r\n");
  }

  void setConfig(){
    setName();
    setPassword();
    setBaudrate();
  }

  cmd_t getCommand(){
    static enum { ST_IDLE, ST_CMD, ST_LEN, ST_DATA, ST_END } state = ST_IDLE;
    static uint32_t timeStart = 0;
    static uint8_t n = 0;
    bool b = false;
    // check for timeout if we are currently mid packet
    if (state != ST_IDLE)
    {
      if (millis() - timeStart >= BT_COMMS_TIMEOUT)
      {
        //sendACK(PKT_ERR_TOUT);
        timeStart = 0;
        state = ST_IDLE;
      }
    }
    // process next char
    if (_bt->available()){
      char ch;
      ch = _bt->read();
      Serial.println(int(ch));
      switch (state){
        case ST_IDLE:   // waiting start character
          if (ch == PKT_START)
          {
            state = ST_CMD;
            timeStart = millis();
            n = 0;
          }
          break;

        case ST_CMD:    // reading command
          cmd.cmd = ch;
          state = ST_LEN;
          break;

        case ST_LEN:
          cmd.dataLen = ch - '0';
          if (n == cmd.dataLen)
            state = ST_END;
          else
            state = ST_DATA;
          break;

        case ST_DATA:
          cmd.data[n] = ch;
          n++;
          if (n >= cmd.dataLen){
            state = ST_END;
          }
          break;

        case ST_END:
          state = ST_IDLE;
          if (ch == PKT_END){
            commandList.add(cmd);
          }
          break;
      }
    }
    return cmd;
  }

  cmd_t nextCommand(){
    return commandList.shift();
  }

  int commandAvailable(){
    return commandList.size();
  }

  String getCommandData(cmd_t c){
    String s = "";
    for(int i = 0; i < c.dataLen; i++){
      s += c.data[i] - '0';
      }
      return s;
  }

  void printCommand(cmd_t c){
    String s = "";
    s = "\nCommand: ";
    s += (char) c.cmd;
    s += "\nLength: ";
    s += c.dataLen;//
    s += "\nData: ";
    for(int i = 0; i < c.dataLen; i++){
      s += c.data[i] - '0';
    }
    s += '\n';
    Serial.println(s);
  }

  void sendError(char c){
    static char msg[] = { PKT_START, 'Z', '?', PKT_END, '\n', '\0' };

    msg[2] = c;
    //PRINT("\nResp: ", msg);
    _bt->print(msg);
    _bt->flush();
  }

  void sendDataInt(int data, unsigned long timestamp){
	  //char buffer [10];
	  //itoa(data, buffer, 10);
	  //Serial.println(buffer);
    _bt->print("*T");
    _bt->print(timestamp);
    _bt->print("F");
    _bt->print(data);
    _bt->print('~');
	  _bt->flush();
  }
  int sendForce(long force, unsigned long timestamp){
    int bytes_written = 0;
    byte byteArray[60];
    //start, crc, T, timestamp, F, force, ~, string end -> maxLength = 1 + 1 + 1 + 10 + 1 + 11 + 1 + 1 = 27
    uint8_t maxLength = 30;
    char buffer[maxLength] = "*";
    sprintf(buffer, "*T%luF%ld~", timestamp, force);

	  //char buffer [10];
	  //itoa(data, buffer, 10);
	  //Serial.println(buffer);
    bytes_written = _bt->print(buffer);
    bytes_written += _bt->print(CRC8((byte *)buffer, strlen(buffer)));
    //Serial.println(buffer);
    //Serial.println(CRC8((byte *)buffer, strlen(buffer)));
	  _bt->flush();
    return bytes_written;
  }

  byte CRC8(const byte *data, byte len) {
    byte crc = 0x00;
    while (len--) {
      byte extract = *data++;
      for (byte tempI = 8; tempI; tempI--) {
        byte sum = (crc ^ extract) & 0x01;
        crc >>= 1;
        if (sum) {
          crc ^= 0x8C;
        }
        extract >>= 1;
      }
    }
    return crc;
  }


//  void respond(char resp){
//    static char msg[] = { PKT_START, 'A', '0', PKT_END, '\n', '\0' };
//
//    msg[2] = resp;
//    //debug("\nResp: ", msg);
//    _bt->print(msg);
//    _bt->flush();
//  }
  private:
    // Serial interface parameters
    uint8_t _pinRX, _pinTX;
    uint16_t _baud;
    uint8_t state;
    cmd_t cmd;
    SoftwareSerial *_bt;
    LinkedList<cmd_t> commandList; // = LinkedList<cmd_t*>();
//    bool _debug = false;
    /*void debug(const String& s, const char c){
      if (_debug){
        Serial.print(s);
        Serial.println(c);
      }
    }*/
};
