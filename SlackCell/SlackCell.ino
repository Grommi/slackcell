#include "HX711.h"
#include "BT.h"
#include <U8x8lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif

#define OLED_RESET -1

//  ARDUINO ||  PIN   DEVICE
//      A4  ||  SDA   OLED
//      A5  ||  SCK   OLED

//      D10 ||  SCK   HX711
//      D12 ||  DOUT  HX711

//      D4  ||  RX    HC-0x
//      D5  ||  TX    HC-0x

#define DEBUG 0
#define BT_RX  4 //RX on bluetooth module
#define BT_TX  5 //TX on bluetooth module
//#define BT_RX  6 //RX on bluetooth module
//#define BT_TX  8 //TX on bluetooth module
const long baud = 38400;

// HX711 circuit wiring
const int LOADCELL_SCK_PIN = 3;
const int LOADCELL_DOUT_PIN = 2;
const long LOADCELL_OFFSET = 10900;
const long LOADCELL_DIVIDER_N = -445;
const long LOADCELL_DIVIDER_kg = LOADCELL_DIVIDER_N * 9.81;
const long LOADCELL_DIVIDER_lb = LOADCELL_DIVIDER_N * 4.448;

HX711 loadcell;
BT bt(BT_RX, BT_TX, baud);
// if you get a noname product maybe you got SH1106 instead of SSD1306
U8X8_SH1106_128X64_NONAME_HW_I2C u8x8(/* reset=*/ U8X8_PIN_NONE);
//U8X8_SSD1306_128X64_NONAME_HW_I2C u8x8(/* reset=*/ U8X8_PIN_NONE);
unsigned long timestamp = 0;
long maxForce = 0;
long force = 0;
long prevForce = 1;
bool recording = false;
char prev_cmd = '0';
cmd_t cmd;


void setup() {
  Serial.begin(baud);
  Serial.println("Welcome to SlackCell!");
  Serial.print("Sketch:   ");   Serial.println(__FILE__);
  Serial.print("Uploaded: ");   Serial.println(__DATE__);
  bt.begin();
  //bt.setConfig();


  u8x8.begin();
  u8x8.setPowerSave(0);
  // fontlist: https://github.com/olikraus/u8g2/wiki/fntlist8x8
  // n: numeric, f: with chars
  u8x8.setFont(u8x8_font_profont29_2x3_n );
  //u8x8.setFont(u8x8_font_inb21_2x4_f );

  //u8x8.drawString(0,0,"Slack");
  //u8x8.drawString(0,4,"Cell");
  u8x8.drawString(0, 0, "11235813");
  u8x8.drawString(0, 4, "21345589");

  loadcell.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  loadcell.set_offset(LOADCELL_OFFSET);
  loadcell.set_scale(LOADCELL_DIVIDER_N);
  force = loadcell.get_units(30);
  if (force < 30) {
    loadcell.tare();
    force = 0;
  }
  //delay(1000);
  u8x8.clear();
}

void loop() {
  bt.getCommand();
  if (bt.commandAvailable()) {
    prev_cmd = cmd.cmd;
    cmd = bt.nextCommand();
    Serial.println(cmd.cmd);
    if (cmd.cmd == 'R') {
      Serial.println("Command Recording");
      recording = true;
      u8x8.clear();
      u8x8.drawString(0, 0, "11235813");
      u8x8.drawString(0, 4, "21345589");
    }
    if (cmd.cmd == 'S') {
      Serial.println("Command Stop Recording");
      recording = false;
      displayForce(int(force), 0);
      displayForce(maxForce, 4);
    }
    Serial.println(cmd.cmd);
    cmd.cmd = '?';
  }

  if (loadcell.is_ready()) {
    force = loadcell.get_units(1);
    timestamp = millis();
    // filter spikes bigger than 1.1kN
    if (abs(force - prevForce) < 1100){
      bt.sendForce(force, timestamp);
      // if new force value available/force changes
      if (force != prevForce && !recording) {
        //display current force
        displayForce(force, 0);
        prevForce = force;
        maxForce = max(abs(force), abs(maxForce));
        // update display with maxForce only if it changes to reduce flickering
        if (maxForce == abs(force))
          displayForce(maxForce, 4);
      }
    }
  }
}

void displayForce(long force, uint8_t line) {
  // Maximum number of letters the screen can display in one row
  uint8_t maxLength = 8;
  // Long range is +-2,147,483,648
  char bufferF[12] = {};
  ltoa(force, bufferF, 10);
  // align right by padding with spaces
  char bufferLCD[maxLength + 1] = {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '\0'};
  for (int i = 0; i < strlen(bufferF); i++) {
    bufferLCD[maxLength - strlen(bufferF) + i] = bufferF[i];
  }
  u8x8.drawString(0, line, bufferLCD);
}
